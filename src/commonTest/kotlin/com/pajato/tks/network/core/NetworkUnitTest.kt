package com.pajato.tks.network.core

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.core.jsonFormat
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals

class NetworkUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default network object, verify behavior`() {
        val network = Network()
        assertEquals(0, network.id)
    }

    @Test fun `When a test network object is serialized and deserialized, verify behavior`() {
        val network = Network()
        val json = jsonFormat.encodeToString(network)
        assertEquals("{}", json)
        assertEquals(0, jsonFormat.decodeFromString<Network>(json).id)
    }
}
