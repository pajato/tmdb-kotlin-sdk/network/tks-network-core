package com.pajato.tks.network.core

import com.pajato.tks.common.core.LOGO_PATH
import com.pajato.tks.common.core.TmdbId
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class Network(
    val headquarters: String = "",
    val homepage: String = "",
    @SerialName(LOGO_PATH)
    val logoPath: String = "",
    val id: TmdbId = 0,
    val name: String = "",
    @SerialName("origin_country")
    val originCountry: String = "",
)
